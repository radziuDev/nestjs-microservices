import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema.createTable('refresh_token', (table: Knex.AlterTableBuilder) => {
    table.integer('userId').unsigned().notNullable().references('user.id').onDelete('CASCADE');
    table.string('token', 64).notNullable().primary();
    table.dateTime('expiresAt').notNullable();
  });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema.dropTable('refresh_token');
}
