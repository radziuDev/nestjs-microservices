import * as argon2 from 'argon2';
import { BadRequestException, Controller, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { RegisterDto } from './dto/register.dto';
import { User } from './interface/user.interface';
import { EmailExistException } from './exception/email-exist.exception';
import { LoginExistException } from './exception/login-exist.exception';
import { LoginDto } from './dto/login.dto';
import { RefreshTokenDto } from './dto/refresh-token.dto';
import { MeDto } from './dto/me.dto';
import { LogoutDto } from './dto/logout.dto';

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) { }

	@MessagePattern({ cmd: 'signup' })
	async signup(@Payload() payload: RegisterDto): Promise<Partial<User>> {
		try {
			const { login, email, password } = payload;
			const hashedPassword = await argon2.hash(password);

			return await this.appService.createUser({
				login,
				email,
				password: hashedPassword,
			});
		} catch (error) {
			switch (error.constructor) {
				case EmailExistException:
					throw new BadRequestException("The provided email was used.");
				case LoginExistException:
					throw new BadRequestException("The provided login was used.");
				default:
					console.log(error)
					throw new InternalServerErrorException("Unknow error");
			}
		}
	}

	@MessagePattern({ cmd: 'login' })
	async login(@Payload() payload: LoginDto): Promise<Record<string, any>> {
		try {
			const subject = await this.appService.validateUser(payload);
			return await this.appService.createAuthTokens(subject);
		} catch (error) {
			switch (error.constructor) {
				case UnauthorizedException:
					throw new UnauthorizedException("A wrong password or login was entered.");
				default:
					console.log(error)
					throw new InternalServerErrorException("Unknow error");
			}
		}
	}

	@MessagePattern({ cmd: 'refreshToken' })
	async refreshToken(@Payload() payload: RefreshTokenDto): Promise<Record<string, any>> {
		try {
			const subject = await this.appService.getUserByRefreshToken(payload.refreshToken);
			await this.appService.logout(payload.refreshToken);
			return await this.appService.createAuthTokens(subject);
		} catch (error) {
			switch (error.constructor) {
				case UnauthorizedException:
					throw new UnauthorizedException();
				default:
					console.log(error)
					throw new InternalServerErrorException();
			}
		}
	}

	@MessagePattern({ cmd: 'me' })
	async getMe(@Payload() payload: MeDto): Promise<Partial<User>> {
		try {
			const user = await this.appService.getUserFromToken(payload.token);	
			return user;
		} catch (error) {
			switch (error.constructor) {
				case UnauthorizedException:
					throw new UnauthorizedException();
				default:
					console.log(error)
					throw new InternalServerErrorException();
			}
		}
	}

	@MessagePattern({ cmd: 'logout' })
	async logout(@Payload() payload: LogoutDto): Promise<unknown> {
		try {
			await this.appService.logout(payload.refreshToken);
			return [];
		} catch (error) {
			switch (error.constructor) {
				case UnauthorizedException:
					throw new UnauthorizedException();
				default:
					console.log(error)
					throw new InternalServerErrorException();
			}
		}
	}
}
