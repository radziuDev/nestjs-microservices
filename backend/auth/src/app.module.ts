import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Filters } from './shared/rpc.exception';
import { KnexModule } from 'nestjs-knex';
import { UserRepository } from './infrastructure/user.repository';
import { RefreshTokenRepository } from './infrastructure/refresh-token.repository';
import { JwtModule } from '@nestjs/jwt';

import Config from './config';
import { CryptoService } from './shared/crypto.service';

@Module({
	imports: [
		KnexModule.forRoot({ config: Config.Database }),
		JwtModule.register({
			secret: Config.Auth.jwtSecret,
			signOptions: {
				expiresIn: `${Config.Auth.jwtExpire}s`
			},
		}),
	],
	controllers: [AppController],
	providers: [
		...Filters,
		AppService,
		UserRepository,
		RefreshTokenRepository,
		CryptoService,
	],
})

export class AppModule { }
