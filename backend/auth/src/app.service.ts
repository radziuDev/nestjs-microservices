import * as argon2 from 'argon2';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { RefreshTokenRepository } from './infrastructure/refresh-token.repository';
import { UserRepository } from './infrastructure/user.repository';
import { User } from './interface/user.interface';
import { LoginDto } from './dto/login.dto';
import { addMilliseconds } from 'date-fns';
import { JwtService } from '@nestjs/jwt';
import { CryptoService } from './shared/crypto.service';

import Config from './config'

@Injectable()
export class AppService {
	constructor(
		private readonly userRepository: UserRepository,
		private readonly refreshTokenRepository: RefreshTokenRepository,
		private readonly jwtService: JwtService,
		private readonly cryptoService: CryptoService,
	) { }

	async createUser(user: {
		login: string,
		email: string,
		password: string,
	}): Promise<Partial<User>> {
		return this.userRepository.save(user);
	}

	async createAuthTokens(user: Partial<User>) {
		const refreshToken = this.cryptoService.generateRefreshToken(Config.Auth.refreshSecret);

		const accessTokenExpireDate = addMilliseconds(new Date(), Config.Auth.jwtExpire * 1000);
		const refreshTokenExpireDate = addMilliseconds(new Date(), Config.Auth.refreshExpire * 1000);

		await this.refreshTokenRepository.persist({
			userId: user.id,
			token: refreshToken,
			expiresAt: refreshTokenExpireDate,
		});

		return {
			jwtToken: {
				token: this.jwtService.sign({
					email: user.email,
					sub: user.id,
				}),
				expiresAt: accessTokenExpireDate,
				cookieOptions: {
					httpOnly: false,
					sameSite: 'none',
					secure: true,
					expires: accessTokenExpireDate,
				},
			},
			refreshToken: {
				token: refreshToken,
				expiresAt: refreshTokenExpireDate,
				cookieOptions: {
					httpOnly: false,
					sameSite: 'none',
					secure: true,
					expires: refreshTokenExpireDate,
				},
			},
		};
	}

	async getUserByRefreshToken(token: string): Promise<User> {
		const refreshToken = await this.refreshTokenRepository.findOneByToken(token);
		if (!refreshToken) throw new UnauthorizedException();

		const user = await this.userRepository.getUserById(refreshToken.userId);
		if (!user) throw new UnauthorizedException();

		return user;
	}

	async validateUser({ login, password }: LoginDto): Promise<Partial<User>> {
		const user = await this.userRepository.getUserByLogin(login);
		if (!user) throw new UnauthorizedException();

		const isPasswordValid = await argon2.verify(user.password, password);
		if (!isPasswordValid) throw new UnauthorizedException();

		const { password: userPass, ...result } = user;
		return result;
	}

	async getUserFromToken(token: string): Promise<Partial<User>> {
		const tokenValue = token.split(' ')[1];
		const value = this.jwtService.decode(tokenValue);
		if (!value) throw new UnauthorizedException();

	  const { id, email, login } = await this.userRepository.getUserById(value.sub); 
		return { id, email, login };
	}

	async logout(token: string): Promise<void> {
		await this.refreshTokenRepository.delete(token);
	}
}
