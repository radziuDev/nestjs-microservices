export default {
  accessTokenType: 'bearer',
  jwtSecret: <string>process.env.AUTH_JWT_SECRET,
  jwtExpire: +process.env.AUTH_JWT_EXPIRE,
  refreshSecret: <string>process.env.AUTH_REFRESH_SECRET,
  refreshExpire: +process.env.AUTH_REFRESH_EXPIRE,
};
