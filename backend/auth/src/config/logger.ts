export default {
  levels: <any[]> process.env.LOGGER_LEVELS?.split(',')?.map(level => level.trim()) || [],
};

