import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class LoginDto {
	@IsNotEmpty({ message: "The 'password' field should not be empty." })
	@IsString({ message: "The value of 'login' field should be a string." })
	@MinLength(4, { message: "The minimum login value should be no less than 4 character." })
	@MaxLength(20, { message: "the maximum login value should be no more than 20 characters." })
	login: string;

	@IsNotEmpty({ message: "The 'password' field should not be empty." })
	@IsString({ message: "The value of 'password' field should be a string." })
	@MinLength(4, { message: "The minimum password value should be no less than 4 character." })
	@MaxLength(20, { message: "the maximum password value should be no more than 20 characters." })
	password: string;
}
