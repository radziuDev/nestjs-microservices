import { IsNotEmpty, IsString } from "class-validator";

export class LogoutDto {
	@IsNotEmpty({ message: "Header field 'refreshToken' can't be empty." })
	@IsString({ message: "Header field 'refreshToken' should be a string." })
	refreshToken: string;
}

