import { Contains, IsNotEmpty, IsString } from 'class-validator';

export class MeDto {
	@IsNotEmpty({ message: "Authorization token not provided." })
	@IsString({ message: "Wrong type of authorization token" })
	@Contains('Bearer' ,{ message: "Wrong type of authorization token" })
	token: string;
}
