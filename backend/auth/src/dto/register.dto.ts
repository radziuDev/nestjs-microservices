import { IsEmail, IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';
import { Match } from 'src/shared/match.decorator';

export class RegisterDto {
	@IsNotEmpty({ message: "The 'password' field should not be empty." })
	@IsString({ message: "The value of 'login' field should be a string." })
	@MinLength(4, { message: "The minimum login value should be no less than 4 character." })
	@MaxLength(20, { message: "the maximum login value should be no more than 20 characters." })
	login: string;

	@IsNotEmpty({ message: "The 'email' field should not be empty." })
	@IsEmail({}, { message: "Email must be a valid email address."})
	email: string;

	@IsNotEmpty({ message: "The 'password' field should not be empty." })
	@IsString({ message: "The value of 'password' field should be a string." })
	@MinLength(4, { message: "The minimum password value should be no less than 4 character." })
	@MaxLength(20, { message: "the maximum password value should be no more than 20 characters." })
	password: string;

	@IsNotEmpty({ message: "The 'password' field should not be empty." })
	@IsString({ message: "The value of 'passwordConfirm' field should be a string." })
	@Match('password', { message: "Confirm password should match password." })
	passwordConfirm: string;
}
