import { Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { User } from '../interface/user.interface';
import { EmailExistException } from 'src/exception/email-exist.exception';
import { LoginExistException } from 'src/exception/login-exist.exception';

@Injectable()
export class UserRepository {
	constructor(@InjectKnex() private readonly db: Knex) { }

	async getUserById(id: number): Promise<User> {
		return this.db('user')
			.where('id', id)
			.returning('*')
			.first();
	}

	async getUserByEmail(email: string): Promise<User> {
		return this.db('user')
			.where('email', email)
			.returning('*')
			.first();
	}

	async getUserByLogin(login: string): Promise<User> {
		return this.db('user')
			.where('login', login)
			.returning('*')
			.first();
	}

	async save(user: Partial<User>): Promise<Partial<User>> {
		if (await this.getUserByEmail(user.email)) {
			throw new EmailExistException();
		}

		if (await this.getUserByLogin(user.login)) {
			throw new LoginExistException();
		}

		const [row]: User[] = await this.db('user')
			.insert(user)
			.returning('*');

		const { password, ...res } = row;
		return res;
	}
}

