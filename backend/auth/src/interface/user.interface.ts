export interface User {
  id: number;
  login: string;
  email: string;
  password: string;
  updatedAt: Date;
  createdAt: Date;
}

