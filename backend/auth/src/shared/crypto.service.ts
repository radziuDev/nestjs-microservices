import * as Crypto from 'crypto';

export class CryptoService {
	generateToken(length = 32): string {
		const lengthInBytes = Math.ceil(length / 2);
		const randomString = Crypto.randomBytes(lengthInBytes);
		return randomString.toString('hex').slice(0, length);
	}

	generateRefreshToken(secret: string): string {
		const random = this.generateToken(128);

		const token = Crypto
			.createHmac('sha256', secret)
			.update(random + Date.now())
			.digest('hex');

		return token;
	}
}
