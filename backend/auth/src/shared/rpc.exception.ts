import { throwError } from 'rxjs';
import {
	Catch,
	ArgumentsHost,
	BadRequestException,
	ExceptionFilter,
	InternalServerErrorException,
	ForbiddenException,
	UnauthorizedException,
	NotFoundException,
	ConflictException,
	NotImplementedException,
	NotAcceptableException,
	MethodNotAllowedException,
	GatewayTimeoutException,
	ServiceUnavailableException,
	PayloadTooLargeException,
	RequestTimeoutException,
	ImATeapotException,
} from '@nestjs/common';
import { APP_FILTER } from '@nestjs/core';

@Catch(BadRequestException)
export class BadRequestFilter implements ExceptionFilter {
	catch(exception: BadRequestException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(InternalServerErrorException)
export class InternalServerErrorFilter implements ExceptionFilter {
	catch(exception: InternalServerErrorException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(ForbiddenException)
export class ForbiddenFilter implements ExceptionFilter {
	catch(exception: ForbiddenException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(UnauthorizedException)
export class UnauthorizedFilter implements ExceptionFilter {
	catch(exception: UnauthorizedException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(NotFoundException)
export class NotFoundFilter implements ExceptionFilter {
	catch(exception: NotFoundException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(ConflictException)
export class ConflictFilter implements ExceptionFilter {
	catch(exception: ConflictException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(NotImplementedException)
export class NotImplementedFilter implements ExceptionFilter {
	catch(exception: NotImplementedException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(NotAcceptableException)
export class NotAcceptableFilter implements ExceptionFilter {
	catch(exception: NotAcceptableException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(MethodNotAllowedException)
export class MethodNotAllowedFilter implements ExceptionFilter {
	catch(exception: MethodNotAllowedException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(GatewayTimeoutException)
export class GatewayTimeoutFilter implements ExceptionFilter {
	catch(exception: GatewayTimeoutException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(ServiceUnavailableException)
export class ServiceUnavailableFilter implements ExceptionFilter {
	catch(exception: ServiceUnavailableException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(RequestTimeoutException)
export class TimeoutFilter implements ExceptionFilter {
	catch(exception: RequestTimeoutException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(PayloadTooLargeException)
export class PayloadTooLargeFilter implements ExceptionFilter {
	catch(exception: PayloadTooLargeException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}

@Catch(ImATeapotException)
export class ImATeapotFilter implements ExceptionFilter {
	catch(exception: ImATeapotException, _host: ArgumentsHost) {
		return throwError(() => exception.getResponse());
	}
}


export const Filters = [
	{
		provide: APP_FILTER,
		useClass: InternalServerErrorFilter,
	}, {
		provide: APP_FILTER,
		useClass: BadRequestFilter,
	},
	{
		provide: APP_FILTER,
		useClass: ForbiddenFilter,
	},
	{
		provide: APP_FILTER,
		useClass: UnauthorizedFilter,
	},
	{
		provide: APP_FILTER,
		useClass: NotFoundFilter,
	},
	{
		provide: APP_FILTER,
		useClass: ConflictFilter,
	},
	{
		provide: APP_FILTER,
		useClass: NotImplementedFilter,
	},
	{
		provide: APP_FILTER,
		useClass: NotAcceptableFilter,
	},
	{
		provide: APP_FILTER,
		useClass: MethodNotAllowedFilter,
	},
	{
		provide: APP_FILTER,
		useClass: GatewayTimeoutFilter,
	},
	{
		provide: APP_FILTER,
		useClass: ServiceUnavailableFilter,
	},
	{
		provide: APP_FILTER,
		useClass: TimeoutFilter,
	},
	{
		provide: APP_FILTER,
		useClass: PayloadTooLargeFilter,
	},
	{
		provide: APP_FILTER,
		useClass: ImATeapotFilter,
	},
];
