import { Controller, InternalServerErrorException } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern, Payload } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

	@MessagePattern({ cmd: 'getMyChats' })
	async getMyChat(@Payload() payload: any): Promise<Record<string, any>> {
		try {
			const data = await this.appService.getHello();
			return { data };
		} catch (error) {
			switch (error.constructor) {
				default:
					console.log(error)
					throw new InternalServerErrorException('Unknow error');
			}
		}
	}
}
