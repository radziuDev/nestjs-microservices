import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Filters } from './shared/rpc.exception';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [
		...Filters,
		AppService,
	],
})
export class AppModule {}
