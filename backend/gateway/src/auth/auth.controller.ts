import { Body, Controller, Inject, Post, Response, Request, Get, UseGuards, Req } from '@nestjs/common';
import { ClientProxy, RpcException } from '@nestjs/microservices';
import { Response as ExpressResponse, Request as ExpressRequest } from 'express';
import { catchError, firstValueFrom, lastValueFrom, throwError } from 'rxjs';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';

@Controller('auth')
export class AuthController {
	constructor(@Inject('AUTH_SERVICE') private client: ClientProxy) { }

	@Post('/signup')
	async signup(@Body() data: Record<string, any>): Promise<unknown> {
		return firstValueFrom(this.client
			.send({ cmd: 'signup' }, data)
			.pipe(catchError(error => throwError(() => new RpcException(error))))
		);
	}

	@Post('/login')
	async login(
		@Response() res: ExpressResponse,
		@Body() data: Record<string, any>,
	): Promise<unknown> {
		let { jwtToken, refreshToken } = await firstValueFrom(this.client
			.send({ cmd: 'login' }, data)
			.pipe(catchError(error => throwError(() => new RpcException(error))))
		);

		const toDate = (obj: Record<string, any>) => {
			obj.cookieOptions.expires = new Date(obj.cookieOptions.expires);
			obj.expiresAt = new Date(obj.expiresAt);
		};

		toDate(jwtToken);
		toDate(refreshToken);

		res.cookie('accessToken', jwtToken.token, jwtToken.cookieOptions);
		res.cookie('accessTokenType', 'bearer', jwtToken.cookieOptions);
		res.cookie('refreshToken', refreshToken.token, refreshToken.cookieOptions);

		return res.send();
	}

	@Post('/refreshToken')
	async refreshToken(
		@Request() req: ExpressRequest,
		@Response() res: ExpressResponse,
	): Promise<unknown> {
		const cookies = req.cookies;
		let { jwtToken, refreshToken } = await firstValueFrom(this.client
			.send({ cmd: 'refreshToken' }, { refreshToken: cookies['refreshToken'] })
			.pipe(catchError(error => throwError(() => new RpcException(error))))
		);

		const toDate = (obj: Record<string, any>) => {
			obj.cookieOptions.expires = new Date(obj.cookieOptions.expires);
			obj.expiresAt = new Date(obj.expiresAt);
		};

		toDate(jwtToken);
		toDate(refreshToken);

		res.cookie('accessToken', jwtToken.token, jwtToken.cookieOptions);
		res.cookie('accessTokenType', 'bearer', jwtToken.cookieOptions);
		res.cookie('refreshToken', refreshToken.token, refreshToken.cookieOptions);

		return res.send();
	}

	@Get('/me')
	@UseGuards(JwtAuthGuard)
	async getMe(@Req() { user }: { user: any }): Promise<unknown> {
		return user;
	}

	@Get('/logout')
	async logout(@Request() req: ExpressRequest): Promise<unknown> {
		return lastValueFrom(this.client
			.send({ cmd: 'logout' }, { refreshToken: req.cookies['refreshToken'] })
			.pipe(catchError(error => throwError(() => new RpcException(error))))
		);
	}
}
