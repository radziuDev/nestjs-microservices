import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

import Config from 'src/config';

@Module({
	controllers: [AuthController],
	providers: [
		{
			provide: 'AUTH_SERVICE',
			useFactory: () => {
				return ClientProxyFactory.create({
					transport: Transport.TCP,
					options: {
						host: Config.AuthService.host,
						port: Config.AuthService.port,
					}
				})
			}
		},
	],
})

export class AuthModule {}
