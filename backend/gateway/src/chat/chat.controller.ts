import { Controller, Inject, Get, UseGuards, Req } from '@nestjs/common';
import { ClientProxy, RpcException } from '@nestjs/microservices';
import { catchError, firstValueFrom, throwError } from 'rxjs';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';

@Controller('chat')
export class ChatController {
	constructor(@Inject('CHAT_SERVICE') private client: ClientProxy) { }

	@Get('/')
	@UseGuards(JwtAuthGuard)
	async getMe(@Req() { user }: { user: any }): Promise<unknown> {
		return firstValueFrom(this.client
			.send({ cmd: 'getMyChats' }, { user })
			.pipe(catchError(error => throwError(() => new RpcException(error))))
		);
	}
}
