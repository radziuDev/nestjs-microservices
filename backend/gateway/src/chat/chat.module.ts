import { Module } from '@nestjs/common';
import { ChatController } from './chat.controller';
import { ClientProxyFactory, Transport } from '@nestjs/microservices';

import Config from 'src/config';

@Module({
	controllers: [ChatController],
	providers: [
		{
			provide: 'CHAT_SERVICE',
			useFactory: () => {
				return ClientProxyFactory.create({
					transport: Transport.TCP,
					options: {
						host: Config.ChatService.host,
						port: Config.ChatService.port,
					}
				})
			}
	  },
	],	
})

export class ChatModule {}
