export default {
	host: process.env.HOST || '127.0.0.1',
	port: +process.env.PORT || 3000,
	prefix: process.env.PREFIX || 'api',
	domain: process.env.FRONTEND_DOMAIN || 'http://localhost:3000',
}
