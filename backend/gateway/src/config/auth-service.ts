export default {
	host: process.env.AUTH_SERVICE_HOST || '127.0.0.1',
	port: +process.env.AUTH_SERVICE_PORT || 3000,
}
