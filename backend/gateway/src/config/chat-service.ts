export default {
	host: process.env.CHAT_SERVICE_HOST || '127.0.0.1',
	port: +process.env.CHAT_SERVICE_PORT || 3000,
}
