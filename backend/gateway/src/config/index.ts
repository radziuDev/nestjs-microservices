import App from './app';
import Logger from './logger';
import AuthService from './auth-service';
import ChatService from './chat-service';

export default {
	App,
	Logger,
	AuthService,
	ChatService,
}
