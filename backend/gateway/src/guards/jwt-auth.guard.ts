import { CanActivate, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, RpcException, Transport } from '@nestjs/microservices';
import { catchError, firstValueFrom, throwError } from 'rxjs';

import Config from '../config';

export class JwtAuthGuard implements CanActivate {
	private client: ClientProxy;

  constructor() {
    this.client = ClientProxyFactory.create({
      transport: Transport.TCP,
      options: {
        host: Config.AuthService.host,
        port: Config.AuthService.port,
      },
    });
  }

	async canActivate(context: ExecutionContext): Promise<boolean> {
		try {
			const request = context.switchToHttp().getRequest();
			const user = await firstValueFrom(this.client
				.send({ cmd: 'me' }, { token: request.headers.authorization })
				.pipe(catchError(error => throwError(() => new RpcException(error))))
			);

			request.user = user;
			return true;
		} catch (error) {
			throw new UnauthorizedException();
		}
	}
}
