require('dotenv').config();

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { RpcExceptionFilter } from './shared/exception-filter';
import * as cookieParser from 'cookie-parser';

import Config from 'src/config';

async function bootstrap() {
	const {
		App: { port, host, prefix, domain },
		Logger: { levels },
	} = Config;

	const app = await NestFactory.create(AppModule, { logger: levels });
	app.enableCors({
    origin: [domain],
    methods: ['*'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true,
  });

	app.useGlobalFilters(new RpcExceptionFilter());
	app.use(cookieParser());

	app.setGlobalPrefix(prefix);
  await app.listen(port, host);
}

bootstrap();
