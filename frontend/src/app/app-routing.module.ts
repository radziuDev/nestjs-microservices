import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { SettingsComponent } from './pages/settings/settings.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: "",
    component: HomeComponent,
  },
  {
    path: "login",
    component: LoginComponent,
    canActivate: [AuthGuard],
    data: { protected: true, logged: false },
  },
  {
    path: "register",
    component: RegisterComponent,
    canActivate: [AuthGuard],
    data: { protected: true, logged: false },
  },
  {
    path: "settings",
    component: SettingsComponent,
    canActivate: [AuthGuard],
    data: { protected: true, logged: true },
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule { }
