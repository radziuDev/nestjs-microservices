import { Component, ViewChild } from '@angular/core';
import { Message, MessageBoxComponent } from './shared/message-box/message-box.component';
import { EventEmiterService } from './event-emiter/event-emiter.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})

export class AppComponent {
  title = 'frontend';
  @ViewChild(MessageBoxComponent) messageBox!: MessageBoxComponent;

  constructor(private eventEmiter: EventEmiterService) {
    this.eventEmiter.notifyEvent$.subscribe(
      (data) => this.notify(data)
    );
  }

  notify(message: Message): void {
    this.messageBox.addMessage(message);
  }
}
