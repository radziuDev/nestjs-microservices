import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MessageBoxComponent } from './shared/message-box/message-box.component';
import { AuthStore } from './store/auth.store';
import { FooterComponent } from './layout/footer.component';
import { HeaderComponent } from './layout/header.component';
import { AuthGuard } from './guards/auth.guard';
import { ConfigService } from './config/config.service';
import { ChatStore } from './store/chat.store';

@NgModule({
  declarations: [
    AppComponent,
    MessageBoxComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    CommonModule,
  ],
  providers: [
    AuthGuard,
    ConfigService,
    AuthStore,
    ChatStore,
  ],
  bootstrap: [AppComponent],
})

export class AppModule {}
