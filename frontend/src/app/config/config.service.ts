import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EventEmiterService } from '../event-emiter/event-emiter.service';

@Injectable({ providedIn: 'root' })
export class ConfigService {
  public config: ConfigStruct = {} as ConfigStruct;

  constructor(
    private http: HttpClient,
    private eventEmiter: EventEmiterService,
  ) {
    this.loadConfig();
  }

  private async loadConfig(): Promise<void> {
    this.http.get('assets/config.json').subscribe((data) => {
      this.config = data as ConfigStruct;
      this.eventEmiter.endConfigEvent();
    });
  }
}

export interface ConfigStruct {
  apiUrl: string,
}

