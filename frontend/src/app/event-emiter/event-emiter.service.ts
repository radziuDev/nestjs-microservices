import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Message } from '../shared/message-box/message-box.component';

@Injectable({ providedIn: 'root' })
export class EventEmiterService {
  private addNotificationSubject = new Subject<Message>();
  private endConfigSubject = new Subject<void>();

  notifyEvent$ = this.addNotificationSubject.asObservable();
  configEvent$ = this.endConfigSubject.asObservable();

  addNotificationEvent(message: Message): void {
    this.addNotificationSubject.next(message);
  }

  endConfigEvent(): void {
    this.endConfigSubject.next();
  }
}
