import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthStore } from '../store/auth.store';

@Injectable({ providedIn: 'root' })
export class AuthGuard {
  constructor(
    private authStore: AuthStore,
    private router: Router,
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    _state: RouterStateSnapshot,
  ): Promise<boolean> {
    return new Promise((res) => {
      const checkInterval = setInterval(() => {
        if (this.authStore.authCompleted) {
          clearInterval(checkInterval);
          const isProtected = route.data['protected'] === true;
          const shouldBeLogged = route.data['logged'];
          const isLoggedIn = !!this.authStore.user;

          if (isProtected && !isLoggedIn === shouldBeLogged) {
            this.router.navigate(['/']);
            res(false);
          }

          res(true);
        }
      }, 50);
    });
  }
}
