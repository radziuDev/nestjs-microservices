import { Component } from '@angular/core';
import { AuthStore } from '../store/auth.store';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})

export class HeaderComponent {
  constructor(public authStore: AuthStore) {}

  logout(): void {
    this.authStore.logout();
  }
}
