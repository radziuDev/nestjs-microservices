import { Component } from '@angular/core';
import { PageComponent } from 'src/app/shared/page/page.component';
import { TitleComponent } from 'src/app/shared/title/title.component';
import { ChatStore } from 'src/app/store/chat.store';

@Component({
  imports: [
    PageComponent,
    TitleComponent,
  ],
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  standalone: true,
})

export class HomeComponent {
  constructor(private chatStore: ChatStore) {}

  testChat() {
    this.chatStore.getChats();
  }
}
