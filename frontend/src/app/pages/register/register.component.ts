import { Component } from '@angular/core';
import { ButtonComponent } from 'src/app/shared/button/button.component';
import { InputComponent } from 'src/app/shared/input/input.component';
import { PageComponent } from 'src/app/shared/page/page.component';
import { TitleComponent, TitleComponentPosition } from 'src/app/shared/title/title.component';
import { FormsModule } from '@angular/forms';
import { AuthStore } from 'src/app/store/auth.store';

@Component({
  imports: [
    FormsModule,
    PageComponent,
    TitleComponent,
    InputComponent,
    ButtonComponent,
  ],
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  standalone: true,
})

export class RegisterComponent {
  titlePosition: typeof TitleComponentPosition = TitleComponentPosition;
  form: Form = {} as Form;

  constructor(
    private authStore: AuthStore,
  ) { }

  updateForm(value: string, field: keyof Form): void {
    this.form[field] = value;
  }

  async onSubmit(): Promise<void> {
    this.authStore.register(this.form);
  }
}

interface Form {
  login: string,
  email: string,
  password: string,
  passwordConfirm: string,
}

