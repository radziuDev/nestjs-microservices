import { Component } from '@angular/core';
import { PageComponent } from 'src/app/shared/page/page.component';
import { TitleComponent } from 'src/app/shared/title/title.component';

@Component({
  imports: [
    PageComponent,
    TitleComponent,
  ],
  selector: 'app-home',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  standalone: true,
})

export class SettingsComponent { }
