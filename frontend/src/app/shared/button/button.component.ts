import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  standalone: true,
})

export class ButtonComponent {
  @Input() type: string = "button";
  @Output() clickEvent = new EventEmitter<Event>();

  emitOnClick(event: Event): void {
    this.clickEvent.emit(event);
  }
}
