import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  standalone: true,
  imports: [
    FormsModule,
  ],
})

export class InputComponent {
  @Input() type: string = 'button';
  @Input() id!: string;
  @Input() name!: string;
  @Input() required: boolean = false;
  @Input() value: any;
  @Output() valueUpdate = new EventEmitter<string>();

  isNotEmpty: boolean = false;

  updateValue(_event: Event): void {
    this.isNotEmpty = this.value.length;
    this.valueUpdate.emit(this.value);
  }
}
