import { Component } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'app-message-box',
  templateUrl: './message-box.component.html',
  styleUrls: ['./message-box.component.scss'],
})

export class MessageBoxComponent {
  messages: Message[] = [];

  addMessage(message: Message): void {
    message.id = uuidv4();
    if (this.messages.length >= 3) this.messages.shift();
    this.messages.push(message);

    setTimeout(() => {
      const index = this.messages.findIndex(item => item.id === message.id);
      if (index !== -1) this.messages.splice(index, 1);
    }, 3000);
  }
}

export interface Message {
  id?: string | null,
  message: string,
  type: 'info' | 'error' | 'success'
}
