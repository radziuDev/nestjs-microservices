import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY, catchError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ConfigService } from '../config/config.service';
import { EventEmiterService } from '../event-emiter/event-emiter.service';

@Injectable({ providedIn: 'root' })
export class AuthStore {
  private _user: User | undefined;
  private _authCompleted: boolean = false;
  private configLoaded: boolean = false;

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private eventEmiter: EventEmiterService,
    private router: Router,
    private cookieService: CookieService,
  ) {
    this.eventEmiter.configEvent$.subscribe(() => {
      this.configLoaded = true;
      this.refreshToken()
    });

    if (this.configLoaded) this.refreshToken();
  }

  get user(): User | undefined {
    return this._user;
  }

  get authCompleted(): boolean {
    return this._authCompleted;
  }

  set setUser(user: undefined | User) {
    this._user = user;
    this._authCompleted = true;
    if (!user) {
      this.cookieService.delete('accessToken');
      this.cookieService.delete('refreshToken');
    }
  }

  set setAuthCompleted(isCompleted: boolean) {
    this._authCompleted = isCompleted;
  }

  register(form: {
    login: string,
    email: string,
    password: string,
    passwordConfirm: string,
  }): void {
    this.http.post<void>(
      `${this.configService.config.apiUrl}/auth/signup`,
      form,
      { withCredentials: true },
    )
      .pipe(catchError(async (error: any) => await this.throwError(error)))
      .subscribe(() => {
        this.eventEmiter.addNotificationEvent({
          message: 'Successfully created an account.',
          type: 'success',
        });

        this.router.navigate(['/login']);
      });
  }

  login(form: { login: string, password: string }): void {
    this.setAuthCompleted = false;
    this.http.post<void>(
      `${this.configService.config.apiUrl}/auth/login`,
      form,
      { withCredentials: true },
    )
      .pipe(catchError(async (error: any) => await this.throwError(error)))
      .subscribe(() => {
        this.eventEmiter.addNotificationEvent({
          message: 'Successfully logged in.',
          type: 'success',
        });

        this.fetchUser();
        this.router.navigate(['/']);
      });
  }

  refreshToken(): void {
    if (!this.cookieService.get('refreshToken')) return this.setUser = undefined;
    this.setAuthCompleted = false;
    this.http.post<void>(
      `${this.configService.config.apiUrl}/auth/refreshToken`,
      null,
      { withCredentials: true },
    )
      .pipe(catchError(async (error: any) => await this.throwError(error)))
      .subscribe(() => this.fetchUser());
  }

  fetchUser(): void {
    this.setAuthCompleted = false;
    this.http.get<void>(
      `${this.configService.config.apiUrl}/auth/me`,
      {
        withCredentials: true,
        headers: {
          Authorization: `Bearer ${this.cookieService.get('accessToken')}`,
        },
      },
    )
      .pipe(catchError(async (error: any) => await this.throwError(error)))
      .subscribe((user: any) => {
        this.setUser = {
          id: user['id'],
          login: user['login'],
          email: user['email'],
        };
      });
  }

  logout(): void {
    this.setAuthCompleted = false;
    this.http.get<void>(
      `${this.configService.config.apiUrl}/auth/logout`,
      { withCredentials: true },
    ).pipe(catchError(() => EMPTY)).subscribe(() => {
      this.setUser = undefined;
      this.router.navigate(['/']);
    });
  }

  async throwError(error: any) {
    const errorMessage = Array.isArray(error.error.message)
      ? error.error.message[0]
      : error.error.message;

    this.eventEmiter.addNotificationEvent({
      message: errorMessage,
      type: 'error',
    });

    this.setUser = undefined;
    throw error;
  }
}

interface User {
  id: number,
  login: string,
  email: string,
}
