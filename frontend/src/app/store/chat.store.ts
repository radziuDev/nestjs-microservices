import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs';
import { ConfigService } from '../config/config.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable({ providedIn: 'root' })
export class ChatStore {

  constructor(
    private http: HttpClient,
    private configService: ConfigService,
    private cookieService: CookieService,
  ) { }

  getChats(): void {
    const accessToken = this.cookieService.get('accessToken');
    this.http
      .get<void>(`${this.configService.config.apiUrl}/chat`, {
        withCredentials: true, headers: {
          Authorization: `Bearer ${accessToken}`,
        }
      })
      .pipe(catchError(error => { throw error }))
      .subscribe((data: any) => console.log(data));
  }
}

